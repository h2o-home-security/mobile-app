# Building Steps

### - Install Node JS
Download and install Node JS from Here https://nodejs.org/

### - Installl Expo Cli
Open command line in the system and excute:
```sh
npm install expo-cli --global
```

### - Cloning and Install Dependencies
open the command line in an empty folder and excute:
```sh
git clone https://gitlab.com/h2o-home-security/mobile-app.git
cd mobile-app/
npm install
```

### - Test and Build
install the Expo Client App on your mobile phone from https://expo.io/tools
Test the app using:
```sh
expo start
```
and follow the inststructions on the screen
or, 
Build the App using:
```sh
expo ba
```