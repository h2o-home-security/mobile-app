import React from 'react';

import * as Expo from 'expo';

import * as ReactNative from 'react-native';
import { Root, Container, Header, Content, Footer, Form, Item, Left, Body, Right, Title, View, Button, Icon, Label, Text, Input } from 'native-base';


// This refers to the function defined earlier in this guide
import registerForPushNotificationsAsync from './src/registerForPushNotificationsAsync';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      AppReady: false,
      temp: "",
      notification: {},
      token: "",
      ip: "192.168.1.2"
    };
  }

  componentWillMount() {
    var that = this;
    registerForPushNotificationsAsync().then(function(value) {
      console.log(value); // "Success"
      that.setState({token: value});
    }, function(e) {
      console.log(e); // TypeError: Throwing
    });
    this._notificationSubscription = Expo.Notifications.addListener(this._handleNotification);
  }

  _handleNotification = (notification) => {
    console.log(notification);
    this.setState({notification: notification});
  };

  _changeSolenoid = (state) => {
    console.log("http://"+ this.state.ip + "/?triggered=" + state);
    fetch("http://"+ this.state.ip + "/?triggered=" + state);
  }

  _connect = false;
  _fetchTemp = () => {
    fetch("http://"+ this.state.ip + "/temp")
    .then((response) => {
      // console.log(response);
      return response.json();
    })
    .then((responseJson) => {
      // console.log(responseJson)
      this.setState({temp: "Temp = "+ responseJson.Temp +"°C"});
      if(this._connect)
      setTimeout(this._fetchTemp, 100);
    })
    .catch((error) => {
      console.log(error);
      this.setState({temp: ""});
      if(this._connect)
      setTimeout(this._fetchTemp, 100);
    });
  }
  render() {
    if (!this.state.AppReady) {
      return (<Expo.AppLoading 
        startAsync={this._loadResourcesAsync}
        onError={this._handleLoadingError}
        onFinish={this._handleFinishLoading}
      />);
    }
    return (
      <Root>
        {<ReactNative.View style={{
          height: Expo.Constants.statusBarHeight,
        }} />}
        <Container>
      {<Header>

        <Body style={{ flex: 1, alignItems: 'center'}}>
          <Title>H2O Home Security</Title>
        </Body>

      </Header>}
      
      <Content padder contentContainerStyle={{ flex: 1}}>
        <Form>
            <Item floatingLabel>
            <Label>Server IP</Label>
              <Input 
                value={this.state.ip}
                onChangeText={(ip) => this.setState({ip})}
                 />
            </Item>
            <Item floatingLabel>
              <Label>Expo Notification Token</Label>
              <Input 
              value={(this.state.token)? this.state.token : "Check Internet Connection"}
              editable = {false} />
            </Item>
          </Form>
        <Body style={{alignSelf: 'center', paddingTop: 50,flexDirection: "column" , justifyContent: 'space-between'}}>
        <View style={{flex: 1,  alignSelf: 'center', flexDirection: "row" , justifyContent: 'space-between'}}>
        <Button style={{marginHorizontal: 50}}
            onPress={()=> {
              this._changeSolenoid("0");
              }}>
            <Text>Solenoid ON</Text>
          </Button>
          <Button style={{marginHorizontal: 50}}
            onPress={()=> {
              this._changeSolenoid("1");
              }}>
            <Text>Solenoid OFF</Text>
          </Button>
          </View>
          <View style={{flex: 3, alignSelf: 'center', justifyContent: 'flex-start', alignContent: 'center'}}>
          <Button style={{alignSelf: 'center', marginHorizontal: 50}}
            onPress={()=> {
              if(this._connect){
                this._connect = false;
                this._fetchTemp();
              }else{
                this._connect = true;
                this._fetchTemp();
              }
              }}>
            <Text>{(this._connect) ? "Disconnect Temperature" : "Connect Temperature"}</Text>
          </Button>
          <Label  style={{alignSelf: 'center', margin: 20, fontSize: 20}} > {(this._connect && this.state.temp) ? this.state.temp : "No Connection"}
          </Label>
        </View>
        <Text> ©2019 Taz Mojo </Text>
        </Body>
      </Content>
    </Container>
      </Root>
    );
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Expo.Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require("native-base/Fonts/Ionicons.ttf"),
        // Entypo: require("native-base/Fonts/Entypo.ttf"),
        // Feather: require("native-base/Fonts/Feather.ttf"),
        // FontAwesome: require("native-base/Fonts/FontAwesome.ttf"),
        // MaterialIcons: require("native-base/Fonts/MaterialIcons.ttf"),
        MaterialCommunityIcons: require("native-base/Fonts/MaterialCommunityIcons.ttf"),
        // Octicons: require("native-base/Fonts/Octicons.ttf"),
        // Zocial: require("@expo/vector-icons/fonts/Zocial.ttf"),
        // SimpleLineIcons: require("native-base/Fonts/SimpleLineIcons.ttf"),
        // EvilIcons: require("native-base/Fonts/EvilIcons.ttf"),
        // ...Ionicons.font,
      })
    ]);
  };

  _handleLoadingError = error => {
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ AppReady: true });
  };
}
